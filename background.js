function init() {
  // Einstellungen ziehen, ContentScript registrieren
  browser.storage.local.get({
    active: true,
    entries: [],
  })
    .then((storage) => {
      storage.active ? enable() : disable();

      storage.entries.forEach((entry) => {
        registerContentScript(entry.url);
      });
    });
}

init();

function enable() {
  browser.browserAction.setIcon({ path: "icons/swagger.png" });
  browser.storage.local.set({ active: true });
}

function disable() {
  browser.browserAction.setIcon({ path: "icons/swagger_off.png" });
  browser.storage.local.set({ active: false });
}

async function registerContentScript(url) {
  return await browser.contentScripts.register({
    js: [{ file: "content.js" }],
    matches: ["*://" + url.split(":")[0] + "/swagger/*"],
    runAt: "document_start",
  });
}

// pageAction anzeigen
browser.runtime.onMessage.addListener((data, sender) => {
  if (data === "showPageAction") {
    browser.pageAction.show(sender.tab.id);
  }
});

// Storage onChange listener
browser.storage.onChanged.addListener((changes) => {
  if ("entries" in changes) {
    console.log("Changes", changes);
    for (let i = 0; i < changes.entries.newValue.length; i++) {
      registerContentScript(changes.entries.newValue[i].url);
    }
  }
});

// Wenn Icon geklickt wird Blocker de-/aktivieren
browser.browserAction.onClicked.addListener(() => {
  browser.storage.local.get("active").then((store) => {
    store.active ? disable() : enable();
  });
});

// URL Icon Click --> Nothing
browser.pageAction.onClicked.addListener(() => { });
