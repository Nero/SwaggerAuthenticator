console.log("Swagger Authenticator aktiv");
let store;
let interval;

document.addEventListener("DOMContentLoaded", () => {
  browser.storage.local.get({
    active: true,
    entries: [],
    openPostPut: false,
  })

    .then((storage) => {
      store = storage;
      let matchingStorageObject = storage.entries.find((elem) =>
        new RegExp(elem.url).test(location.href)
      );
      let apiKey;
      let portOfStorageObject;
      if (matchingStorageObject) {
        apiKey = matchingStorageObject.apiKey;
        portOfStorageObject = matchingStorageObject.url.split(":")[1];
      }

      if (
        storage.active &&
        apiKey &&
        ((portOfStorageObject && portOfStorageObject == location.port) ||
          (portOfStorageObject == undefined && location.port == ""))
      ) {

        // Show small icon in url bar
        browser.runtime.sendMessage("showPageAction");

        // Check if button has loaded
        interval = setInterval(async () => {

          // Open modal if exists
          const button = document.getElementsByClassName("btn authorize unlocked")[0];
          if (button) {
            await button.click();
            clearInterval(interval);

            // Listener für Formulare setzen -> Event PreventDefault
            let forms = document.getElementsByClassName(
              "btn modal-btn auth authorize button"
            );
            for (let i = 0; i < forms.length; i++) {
              forms[i].form.addEventListener("submit", (event) =>
                event.preventDefault()
              );
            }

            // Keys eintragen
            let inputFields = document.getElementsByTagName("input");
            for (let i = inputFields.length - 1; i > -1; i--) {
              inputFields[i].focus();
              for (var j = 0; j < apiKey.length; j++) {
                inputFields[i].value += apiKey.charAt(j);
                inputFields[i].dispatchEvent(
                  new Event("input", { bubbles: true })
                );
              }
            }

            // Formular absenden durch Buttonclicks
            let authBtns = document.getElementsByClassName(
              "btn modal-btn auth authorize button"
            );
            for (let i = authBtns.length - 1; i > -1; i--) {
              authBtns[i].click();
            }
            document.getElementsByClassName("btn modal-btn auth btn-done button")[0].click();
          }
        }, 500);
      }
    });
});

// Listen for click on every method
let amountOfTryOutBtns = 0;
document.addEventListener("click", () => {
  if (store.active) {
    setTimeout(() => {
      let tryOutBtns = document.getElementsByClassName("btn try-out__btn");
      if (tryOutBtns.length != amountOfTryOutBtns) {
        amountOfTryOutBtns = tryOutBtns.length;

        if (store.openPostPut) {
          for (let i = 0; i < tryOutBtns.length; i++) {
            setTimeout(() => {
              if (
                !tryOutBtns[i].classList.contains("reset") &&
                !tryOutBtns[i].classList.contains("cancel")
              ) {
                tryOutBtns[i].click();
              }
            }, 200);
          }
        } else {
          for (let i = 0; i < tryOutBtns.length; i++) {
            if (
              tryOutBtns[i]
                .closest(".opblock")
                .classList.contains("opblock-get") ||
              tryOutBtns[i]
                .closest(".opblock")
                .classList.contains("opblock-delete")
            ) {
              if (
                !tryOutBtns[i]
                  .closest(".opblock")
                  .classList.contains("cancel") &&
                !tryOutBtns[i].closest(".opblock").classList.contains("reset")
              ) {
                tryOutBtns[i].click();
              }
            }
          }
        }

        for (let i = 0; i < tryOutBtns.length; i++) {
          if (
            tryOutBtns[i]
              .closest(".opblock")
              .classList.contains("opblock-get") ||
            tryOutBtns[i]
              .closest(".opblock")
              .classList.contains("opblock-delete") ||
            (store.openPostPut &&
              tryOutBtns[i]
                .closest(".opblock")
                .classList.contains("opblock-post")) ||
            (store.openPostPut &&
              tryOutBtns[i]
                .closest(".opblock")
                .classList.contains("opblock-put"))
          ) {
            if (
              !tryOutBtns[i].classList.contains("cancel") &&
              !tryOutBtns[i].classList.contains("reset")
            ) {
              tryOutBtns[i].click();
            }
          }
        }
      }
    }, 300);
  }
});
