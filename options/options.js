// Default values
const defaultEntries = [
  {
    url: "10.23.90.9",
    apiKey: "test",
  },
  {
    url: "localhost:8000",
    apiKey: "test",
  },
];

// Save options
document.getElementById("saveBtn").addEventListener("click", (e) => {
  e.preventDefault();
  let entries = [];
  let tfAmount = document.querySelectorAll(".textfield").length / 2;

  for (let i = 0; i < tfAmount; i++) {
    const url = document.getElementById("url_" + i).value.replace(/^https?:?\/?\/?/, "").replace("/$");
    const apiKey = document.getElementById("apiKey_" + i).value.trim();
    if (!url || !apiKey) {
      return alert("Bitte alle Felder ausfüllen!");
    }
    entries.push({ url, apiKey });
  }

  browser.storage.local.set({
    openPostPut: openPostPut.checked,
    entries,
  }).then(() => {
    const saveBtn = document.getElementById("saveBtn");
    saveBtn.value = "Gespeichert!";
    saveBtn.disabled = true;
    setTimeout(() => {
      saveBtn.value = "Speichern";
      saveBtn.disabled = false;
    }, 3000);
  });
});

// Add URL
document.getElementById("addBtn").addEventListener("click", () => {
  browser.storage.local.get({
    entries: defaultEntries
  })
    .then((storage) => {
      appendUrl("", "", storage.entries.length);
    });
});

// Restore entries
document.addEventListener("DOMContentLoaded", restoreSettings);
function restoreSettings() {
  browser.storage.local.get({
    entries: defaultEntries,
    openPostPut: false,
  })
    .then((storage) => {
      for (let i = 0; i < storage.entries.length; i++) {
        appendUrl(storage.entries[i].url, storage.entries[i].apiKey, i);
      }

      openPostPut.checked = storage.openPostPut;
    });
}

function appendUrl(url, apiKey, i) {
  let row = urlTable.insertRow();

  let cell1 = row.insertCell(0);
  let cell2 = row.insertCell(1);
  let cell3 = row.insertCell(2);

  const urlInput = document.createElement("input");
  urlInput.id = "url_" + i;
  urlInput.className = "textfield";
  urlInput.placeholder = "URL";
  urlInput.value = url;

  const apiKeyInput = document.createElement("input");
  apiKeyInput.id = "apiKey_" + i;
  apiKeyInput.className = "textfield";
  apiKeyInput.placeholder = "API-Key";
  apiKeyInput.value = apiKey;

  const btn = document.createElement("input");
  btn.setAttribute("data-row", i);
  btn.setAttribute("type", "button");
  btn.setAttribute("value", "x");
  btn.addEventListener("click", removePair);

  cell1.appendChild(urlInput);
  cell2.appendChild(apiKeyInput);
  cell3.appendChild(btn);
}

// Remove entry
function removePair() {
  let index = this.getAttribute("data-row");
  browser.storage.local.get({
    entries: defaultEntries
  }).then((storage) => {
    storage.entries.splice(index, 1);
    urlTable.deleteRow(parseInt(index) + 1);
    browser.storage.local.set({ entries: storage.entries });
  });
}
